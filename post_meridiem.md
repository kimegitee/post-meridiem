# Session Records: Pop's Project Management Coaching

June 13 2018
## Tracking

### Why track
- Tracking is measuring
- What gets measured gets managed

### Key concepts
1. Tasks
Tasks make progress on project
2. Non-tasks
Tasks that a person has to do but does not contribute to project

### Keeping track of progress
- For each day
- For each member
- List:
 - What are this member's tasks?
 - What are this member's non-tasks?

### What to do with this file?
Review for optimization opportunities
