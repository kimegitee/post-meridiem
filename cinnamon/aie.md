
# Create a layout understanding engine

We are interested in extracting structrured content from a photographed document. For example, given a scanned image of a scientific paper, we may want to extract the paper's title, the names of the authors, or the paper's abstract. A key element to the success of this task is the layout understanding step, which is the organization of text in a document into meaningful clusters, such as Headers, Paragraphs, Lines or Words or in the case of tables, Columns, Rows and Cells... Additionally, the layout understanding engine should be capable of explicitly representing the relationships between the entities found in the document, which answers questions such as: which lines belong to which paragraph, and which paragraphs correspond to a certain header... You are invited to tackle the first part of this challenge by fulfilling the steps below:

1. Create your data using arxiv: Download a hundred or so papers in pdf format. Parse these pdfs and extract the characters and their respective bounding box coordinates.

2. Write an algorithm that, given all the characters and each character's bounding box coordinates found in a single page of a pdf document, group the characters into paragraphs. 'Paragraph' here is loosely defined as a block of text that can either be: a single word (for example, the page number in the lower right corner), a phrase (e.g the title, a header..), a complete sentence or multiple sentences (a paragraph in the normal sense). Is case of ambiguity, use your own judgement and provide your rationale.

You are expected to produce the following outputs:

- Working code that accepts a path to an arxiv pdf and writes to disk in the same location the script was invoked debug images that have paragraph bounding boxes visibly drawn on the pdf's original pages. 
- List different approaches you tried. Did it work or not? Why do you think it did or did not?
- Explain your best approach. You should address each of the following points, using as many words as you deem neccesary: 
  - How does it work?
  - Explain the steps you went through to create the solution, including any reference materials. Explain how each step relates to the final solution.
  - Explain the conditions your approach will work well and conditions it will fail

You can complete the assignment in either English or Vietnamese, whichever is comfortable to you. You can use code in any programming languages, any existing libraries, even libraries that target this exact problem. But you still need to be able to thoroughly explain the solutions used in these libraries. Please wrap your solutions in a github repo and submit the link to our HR contact.

![Sample Input](input.png "Input Sample")

![Sample Output](output.png "Output Sample")

