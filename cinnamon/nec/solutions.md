# Status (Project progress - XX%)

## AI modules

### Document classification

- Implementation 100%
- Optimization 0%
- Integration 0%

### Denoise module

- Implementation 100%
- Optimization 60%
- Integration 100%

### Linecut module

- Implementation 100%
- Optimization 90%
- Integration 100%

### Table analysis module

- Implementation 100%
- Optimization 70%
- Integration 0%

### OCR module

- Implementation 100%
- Optimization 100%
- Integration 50%

### Line classification module

- Implementation 90%
- Optimization 90%
- Integration 0%

### Custom rules specific for document type and field

- Implementation 10%
- Optimization 0%
- Integration 0%

## Challenges and solution

Difficulty:

- 1: easy - have existing working solutions
- 2: medium - achievable within 1 month
- 3: hard - achievable within 3 months
- 4: very hard - hard to reach perfect accuracy

Quotation Form

1. Seals cover text:
  - Difficulty: 4
  - Solution:
    - Use list of companies to auto-correct
      - Expected improvement: Works on long company names
      - Expected failures: If company names too short, cannot recover true company's names
    - If color image is available, seals can be identified by color
      - Expected improvement: 95% by char for company name field only with printed, common fonts
      - Expected failure: seals blend in with text, cannot be removed completely
2. Noisy background:
  - Difficulty: 2
  - Solution:
    - Use denoising module
      - Expected improvement: Mild cases can be denoise completely
      - Expected failures: Severe cases cannot be removed completely
3. High number of items for detail field with complex rules:
  - Difficulty: 3
  - Solution:
    - Use identified keywords and relative positions to classify correct detail fields to take
      - Expected improvement: Work for majority of cases
      - Expected failures: Edge cases where layout deviates too much from typical forms

Order Form

1. Table without borders:
  - Difficulty: 2
  - Solution:
    - Write special case handling in table analysis module
      - Expected improvement: Most common cases can be covered
      - Expected failures: Edge cases may be too many to fully cover

Acceptance Certificate

1. Handwritten text or written by stamping
  - Difficulty: 3
  - Solution:
    - Retrain handwriting module with extracted data
      - Expected improvement: Accuracy by char 70%
      - Expected failures: Higher accuracy requires a lot more data

2. Seal area cropping
  - Difficulty: 2
  - Solution:
    - Customize linecut model to detect seal
      - Expected improvement: Accuracy 100%
      - Expected failures: May fail if seal shape is too different

## Current accuracy
Estimated with current solutions
- 90% by char: confidence 80%
- 30% by field: confidence 95%
