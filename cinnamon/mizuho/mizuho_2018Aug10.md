Report improvement:
- More details
- Anticipate sales' questions, answer questions one-by-one
- More frequent update, short updates as an important step is finished

Assumptions:
- I'll assume that sales prefer more details than fewer details
- This means I'll try to answer in as many details as possible

What did you finish this week?
- Fix bprost line bounding box cutting into text
 - What this means: 
  - Better linecut results, better ocr results
 - Any caveats?
  - Added running time of 1 second

- Table analysis
 - What this means:
  - We can tell which cells are in the same row or column

- Line text classification for "sum", "tax", "total"
 - What this means:
  - We can tell if a line contains keywords related to one of three fields above
  - Applying this to lines in tables, we can tell which row and column contains the value
 - Is this a new approach?
  - Yes
 - What is its shortcomings?
  - Some lines contain values and keywords for different fields. Example: postcode and address are often on the same line
  - Some 
 - Why didn't we try this before?

What new things we learned this week?
- Current bprost linecut model works best (vs RnD, Tesseract):
 - Works on table
 - Works on big text
- Bprost model linebox cuts into text
 - Is it a problem?
  - No, it has been fixed


- 
