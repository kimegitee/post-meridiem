% Mizuho Weekly Report - Week 2
%
% June 	22, 2018

## Milestones
To client:

- First report: 2nd July (1 week away)
- Second report: 23th July
- Third report: 6th Aug
- Fourth report: 20th Aug
- Final report: 27th Aug

## Progress
Current status: On Schedule

-----------------------------------------------------------------------
Tasks              Status        Percent       Next step          Owner
-----              ------        -------       ---------          -----
Linecut            Tested        100%          .                  Harry

Ocr                Tested        100%          .                  Nick

Key-value          Done          100%          .                  Nick
single-output
training flow

Key-value          Starting      0%            Add company        Nick
multi-output                                   name output
training flow

Key-value          Doing         75%           Continue           Nancy
labeling for new
training data
(30 samples)

Db search          Done          100%          .                  Tom
for company
on mock data

Db search          Doing         30%           Get output         Tom           
for company                                    on key-value
on customer's                                  result for
DB                                             'tel' field
-----------------------------------------------------------------------

## Risks

---------------------------------------------------------------------------
Risks                        Priority    Solutions            Next step
----------------------       --------    ----------           -------------
Low quality input            High        Noise removal        Review
                                         + enhancement        solutions

Using new key-value          Medium      Use old              Evaluate                              
approach (entity model)	                 approach for         accuracy
may not work with 20                     PoC                  with full
training examples                                             50 samples

Linecut model may            Low         Finetune             Linecut
need to be finetuned	                 models               owner (alexx)
for best result.                                              to retrain 
-----------------------------------------------------------------------

