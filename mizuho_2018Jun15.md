% Mizuho Weekly Report - Week 1
%
% June 	15, 2018

## Milestones
To client:

- First report: 2nd July
- Second report: 23th July
- Third report: 6th Aug
- Fourth report: 20th Aug
- Final report: 27th Aug"

## Progress
Current status: On Schedule

Tasks|Status|Percent|Next step|Owner
-----|------|-------|---------|-----
Linecut|integrated|75%|testing|Harry
Ocr|finished|50%|integrate|Nick
Key-value|labeling|10%|customize model|Nick
Db search|done for company|30%|bank field|Tom
Reporting|not started|0%|pending|Tom

## Risks

-----------------------------------------------------------------------
Risks                        Solutions            Next step
----------------------       ----------           ---------------------
Linecut model may            New labels           RnD to finetune model
need to be finetuned	     done
for best result. 

Using new key-value          Use old                               
approach (entity model)	     approach for 
may not work with 20         PoC
training examples

Value may be outside	     Wata has checked 
database                     all current 
                             customers will be
                             in db -> low risk 
-----------------------------------------------------------------------

